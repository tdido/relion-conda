A conda recipe to build [relion](https://github.com/3dem/relion) with UI and GPU support.

Currently needs to be run in this docker container: [nvidia/cuda:12.0.0-runtime-rockylinux8](https://hub.docker.com/layers/nvidia/cuda/12.0.0-devel-rockylinux8/images/sha256-82527416046c242d70867b7e63d6785b3657f9c2bea61324b70bb548eecc7435?context=explore)
