yum -y install \
    cuda-nvcc-12-0.x86_64 \
    cuda-cudart-devel-12-0.x86_64 \
    cuda-profiler-api-12-0.x86_64 \
    libcufft-devel-12-0.x86_64 \
    libcurand-devel-12-0.x86_64

export OMPI_MCA_opal_cuda_support=true
mkdir build
cd build
cmake \
    -DCUDA_ARCH=80 \
    -DMPI_C_COMPILER=$CONDA_PREFIX/bin/mpicc \
    -DMPI_CXX_COMPILER=$CONDA_PREFIX/bin/mpicxx \
    -DCMAKE_C_COMPILER=$CONDA_PREFIX/bin/gcc \
    -DCMAKE_CXX_COMPILER=$CONDA_PREFIX/bin/g++ \
    -DCMAKE_CXX_FLAGS="-I /usr/local/cuda/targets/x86_64-linux/include/ -L /usr/lib" \
    -DCMAKE_PREFIX_PATH="/usr/local/cuda" \
    -DCMAKE_INSTALL_PREFIX=$PREFIX \
    -DCUDA_CUDART_LIBRARY=/usr/local/cuda/targets/x86_64-linux/lib/libcudart.so \
    -DCUDA_NVCC_EXECUTABLE=/usr/local/cuda/bin/nvcc \
    -DCUDA_cufft_LIBRARY=/usr/local/cuda/targets/x86_64-linux/lib/libcufft.so \
    -DCUDA_curand_LIBRARY=/usr/local/cuda/targets/x86_64-linux/lib/libcurand.so \
    ..
make
make install
